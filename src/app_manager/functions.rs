use iced_aw::native::{
    wrap::{direction, Wrap},
    context_menu::ContextMenu,
};
use iced::{Length, Alignment, widget::{Button, svg::Handle, Svg}, Padding, Font, ContentFit};
use iced::widget::{
    svg,
    row,
    container,
    text,
    column,
    button,
    image,
};
use crate::themes;
use crate::desktop_parser::parser::DesktopStruct;

use super::init::{Route, Message, DesktopFiles, AppInfo};
pub struct MainContent;

impl MainContent{
    pub fn show_applications_panel(apps: Vec<DesktopStruct>) -> iced::widget::Row<'static, Message>{
        apps
        .iter()
        .fold(row!().spacing(10.0), |r_flatpak, file_content|{
            let app_type = if file_content.flatpak.is_empty(){
                DesktopFiles::System
            } else{
                DesktopFiles::Flatpak
            };
            let exec = if file_content.flatpak.is_empty(){
                file_content.exec.clone()
            } else{
                file_content.flatpak.clone()
            };
          
            let name = file_content.name.clone();
            let icon = file_content.icon.clone();
            let file_name = file_content.file_name.clone();
            let is_hide = file_content.nodisplay_iced;
            r_flatpak.push(
                ContextMenu::new(
                    container(
                        button(
                            Self::show_application_info(app_type.clone(), name.clone(), icon.clone())
                        )
                        .style(iced::theme::Button::Custom(Box::new(themes::buttons::ElemButtonTheme)))
                        .on_press(Message::RunApp(app_type.clone(), exec.clone())
                        )
                        .width(Length::Fixed(110.0))
                        .height(Length::Fixed(110.0))
                    ),
                    
            move || {
                Self::show_context_menu( AppInfo{is_hide, file_name: file_name.clone(), name: name.clone(), app_type: app_type.clone(), exec: exec.clone(), icon: icon.clone()}).into()
            }
        ))
        })
    }
    
    pub fn show_applications(apps: Vec<DesktopStruct>) -> iced_aw::Wrap<'static, Message,direction::Horizontal, iced::Renderer>{
        apps
        .iter()
        .fold(Wrap::<'static, Message, direction::Horizontal, iced::Renderer>::new().spacing(10.0), |r_flatpak, file_content|{

            let app_type = if file_content.flatpak.is_empty(){
                DesktopFiles::System
            } else{
                DesktopFiles::Flatpak
            };
            let exec = if file_content.flatpak.is_empty(){
                file_content.exec.clone()
            } else{
                file_content.flatpak.clone()
            };
          
            let name = file_content.name.clone();
            let icon = file_content.icon.clone();
            let file_name = file_content.file_name.clone();
            let is_hide = file_content.nodisplay_iced;
            r_flatpak.push(
                ContextMenu::new(
                    container(
                        button(
                            Self::show_application_info(app_type.clone(), name.clone(), icon.clone())
                        )
                        .style(iced::theme::Button::Custom(Box::new(themes::buttons::ElemButtonTheme)))
                        .on_press(Message::RunApp(app_type.clone(), exec.clone())
                        )
                        .width(Length::Fixed(110.0))
                        .height(Length::Fixed(110.0))
                    ),
                    
            move || {
                Self::show_context_menu( AppInfo{is_hide, file_name: file_name.clone(), name: name.clone(), app_type: app_type.clone(), exec: exec.clone(), icon: icon.clone()}).into()
            }
        ))
        })
    }
    fn show_application_info(app_type: DesktopFiles, name: String, icon: String) -> iced::widget::Column<'static, Message>{
        column!(
            image(&format!("/home/rscasm/.config/iced-appmanager/images/{}.png", icon))
            .height(50)
            .width(50),
            text(name.clone())
            .style(iced::theme::Text::Color(iced::color!(44, 44, 44)))
            .horizontal_alignment(iced::alignment::Horizontal::Center)  
        )
        .align_items(Alignment::Center)
    }

    fn show_context_menu(app_info: AppInfo) -> iced::widget::Container<'static, Message>{
        let text_action = if app_info.is_hide{
            "Показать"
        } else {
            "Скрыть"   
        };
        
        let context_menu = match app_info.app_type{
            DesktopFiles::Flatpak => {
                column!(
                    Self::main_button(
                        "assets/icons/run.svg".to_string(),
                        "Запустить".to_string(), 
                        Message::RunApp(app_info.app_type.clone(), app_info.exec.clone()), 
                        iced::theme::Button::Custom(Box::new(themes::buttons::SideBarButtonTheme))
                    ),
                    Self::main_button(
                        "assets/icons/run.svg".to_string(),
                        "Добавить на панель".to_string(), 
                        Message::AddOnPanel(app_info.file_name.clone()), 
                        iced::theme::Button::Custom(Box::new(themes::buttons::SideBarButtonTheme))
                    ),
                    Self::main_button(
                        "assets/icons/hide.svg".to_string(),
                        text_action.to_string(), 
                        Message::HideApp(app_info.file_name.clone()), 
                        iced::theme::Button::Custom(Box::new(themes::buttons::SideBarButtonTheme))
                    ),
                    Self::main_button(
                        "assets/icons/delete.svg".to_string(),
                        "Удалить".to_string(), 
                        Message::DeleteApp(app_info.exec), 
                        iced::theme::Button::Custom(Box::new(themes::buttons::SideBarButtonTheme))
                    ),
                )
                .align_items(Alignment::Center)
            },
            DesktopFiles::System => { 
                column!(
                    Self::main_button(
                        "assets/icons/run.svg".to_string(),
                        "Запустить".to_string(), 
                        Message::RunApp(app_info.app_type.clone(), app_info.exec.clone()), 
                        iced::theme::Button::Custom(Box::new(themes::buttons::SideBarButtonTheme))
                    ),
                    Self::main_button(
                        "assets/icons/run.svg".to_string(),
                        "Добавить на панель".to_string(), 
                        Message::AddOnPanel(app_info.file_name.clone()), 
                        iced::theme::Button::Custom(Box::new(themes::buttons::SideBarButtonTheme))
                    ),
                    Self::main_button( 
                        "assets/icons/hide.svg".to_string(),
                        text_action.to_string(), 
                        Message::HideApp(app_info.file_name.clone()), 
                        iced::theme::Button::Custom(Box::new(themes::buttons::SideBarButtonTheme))
                    ),
                )
                .align_items(Alignment::Center)
            }
        };
        container(context_menu)
        .padding(Padding::from(8.0))
        .center_x()
        .center_y()
        .max_width(200)
        .width(200)
        .style(iced::theme::Container::Custom(Box::new(themes::containers::ContextMenuTheme)))
    }
    
    pub fn main_button(svg_path: String, content: String, message: Message, style: iced::theme::Button) -> Button<'static, Message>{
        button(
            row!(
                // svg(Handle::from_path(svg_path))
                // .width(19)
                // .height(19),
               
                text(content) 
                .style(iced::theme::Text::Color(iced::color!(44,44,44)))
                .size(20.0)
            )
            .padding(Padding::from([0, 8]))
            .spacing(8)
            .align_items(Alignment::Center)
            .height(Length::Fill)
        )
        
        .width(194)
        .style(style)            
        .on_press(message)
    }
}