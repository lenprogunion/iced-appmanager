use iced::{
    window,
    executor,
    subscription, Size
};

use iced::{
    Application,
    Command,
    Element,
    Length,
    Alignment,
    Subscription,
    Event,
};

use iced::widget::{
    text_input,
    container,
    text,
    row,
    column,
    button,
    toggler, 
};
use iced_aw::{tabs, Tabs, Modal, modal, Card};
use std::{process};

use crate::desktop_parser::manager::{convert_image, copy_files};
use crate::desktop_parser::{parser,manager, settings_parser};
use crate::app_manager::functions::MainContent;

#[derive(Debug, Clone)]
pub struct AppInfo{
    pub name: String,
    pub app_type: DesktopFiles,
    pub exec: String,
    pub icon: String,
    pub file_name: String,
    pub is_hide: bool,
}
#[derive(Debug,Clone, PartialEq, Default)]
pub enum DesktopFiles{
    #[default]
    Flatpak,
    System,
}

#[derive(Clone, PartialEq, Debug, Default)]
pub enum Route {
    Hidden,
    #[default]
    Main,
}

pub struct AppManager{
    pub path_to_desktops: String,
    pub path_to_settings: String,
    pub path_to_images: String,
    pub is_active_modal: bool,
    pub apps: Vec<parser::DesktopStruct>,
    pub panel_apps: Vec<parser::DesktopStruct>,
    pub settings: crate::desktop_parser::settings_parser::Settings,
    pub current_page: Route,
    pub search_result: Option<Vec<parser::DesktopStruct>>,
    pub search_text: String,
    pub is_hide: bool,
}

#[derive(Debug, Clone)]
pub enum Message {
    RunApp(DesktopFiles,String),
    ToggleWithHide(bool),
    DeleteApp(String),
    CloseMenu(Event),
    Search(String),
    HideApp(String),
    ShowPage(Route),
    ShowSettings,
    ShowManager,
    HideSettings,
    ChangeSize(Size<u32>),
    AddOnPanel(String)
}

impl Application for AppManager {
    type Message = Message;
    type Theme = iced::theme::Theme;
    type Executor = executor::Default;
    type Flags = ();

    fn new(_flags: ()) -> (AppManager, Command<Message>) {
        let path_to_desktops = manager::create_config_dir().into_os_string().into_string().unwrap();
        let path_to_settings = manager::create_settings_file().into_os_string().into_string().unwrap();
        let settings = settings_parser::settings_parser(path_to_settings.clone());
        let apps = parser::desktop_parser(settings.with_hide, path_to_desktops.clone());
        let path_to_images = manager::create_image_directory();
        
        (
            AppManager{
               apps: apps.0,
               panel_apps: apps.1,
               path_to_desktops,
               path_to_settings,
               path_to_images: path_to_images.into_os_string().into_string().unwrap(),
               settings,
               is_hide: true,
               is_active_modal: false,
               current_page: Route::Main,
               search_result: None,
               search_text: String::new(),
            }, 
            Command::none()
        )
    }
    fn title(&self) -> String {
        String::from("Messager")
    }
    fn update(&mut self, message: Message) -> Command<Message> {
        match message {
            Message::RunApp(app_type, app_name) => {
                match app_type{
                    DesktopFiles::Flatpak => {
                        process::Command::new("flatpak")
                        .args(&["run",&app_name])
                        .spawn().unwrap();
                    },
                    DesktopFiles::System => {
                        let args: Vec<&str> = app_name.split(' ').collect();
                        process::Command::new(&args[0])
                        .spawn().unwrap();
                    }
                }
                Command::none()
            },
            Message::CloseMenu(event) => {
                let e = iced::keyboard::Modifiers::empty();
                match event{
                    Event::Keyboard(
                        iced::keyboard::Event::KeyPressed{
                            key_code: iced::keyboard::KeyCode::Escape,
                            modifiers: e
                        }
                    ) => {
                        if !self.is_hide{
                            self.is_hide = true;
                            return window::fetch_size(Message::ChangeSize);
                        }
                        
                        Command::none()
                    },
                    _ => {
                        Command::none()
                    }
                }
            },
            Message::DeleteApp(exec) => {
                process::Command::new("flatpak")
                .args(&["remove", &exec, "-y"])
                .spawn().unwrap();
                manager::delete_desktop_file(self.path_to_desktops.clone() + "/" + &exec);
                Command::none()
            },
            Message::Search(name) => {
                self.search_text = name.clone();
                if name.is_empty(){
                    self.search_result = None;
                    return Command::none();
                }
                let apps = self.apps.clone().iter()
                    .fold(Vec::<parser::DesktopStruct>::new(), |mut c_flatpak: Vec<parser::DesktopStruct>, app_name | {
                        if app_name.name.to_lowercase().contains(&name.to_lowercase()){
                            c_flatpak.push(
                                app_name.clone()
                            );
                        }
                        c_flatpak
                    }
                );
                self.search_result = Some(apps);
                Command::none()
            },
            Message::HideApp(name) => {
                manager::toggle_property(manager::Toggle::Hide, format!("{}/{}", self.path_to_desktops, name.clone()));
                let apps = parser::desktop_parser(self.settings.with_hide, self.path_to_desktops.clone());
                self.apps = apps.0;
                self.panel_apps = apps.1;
                Command::none()
            },
            Message::ShowPage(r) =>{
                self.current_page = r;
                Command::none()
            },
            Message::ShowManager => {
                self.is_hide = false;
                window::fetch_size(Message::ChangeSize)
            },
            Message::ToggleWithHide(with_hide) => {
                self.settings.with_hide = with_hide;
                let apps = parser::desktop_parser(self.settings.with_hide, self.path_to_desktops.clone());
                self.apps = apps.0;
                self.panel_apps = apps.1;
                manager::toggle_property(manager::Toggle::WithHide, self.path_to_settings.clone());
                Command::none()
            },
            Message::ShowSettings => {
                self.is_active_modal = true;
                Command::none()
            },
            Message::HideSettings => {
                self.is_active_modal = false;
                Command::none()
            },
            Message::ChangeSize(size) => {
                let height = if size.height >= 60{
                    50
                } else{
                    700
                };
                Command::batch([
                    window::resize::<Message>(Size::new(size.width, height)),
                    window::move_to((1366-size.width) as i32 /2 , 766)
                ])
            },
            Message::AddOnPanel(path) => {
                manager::toggle_property(manager::Toggle::OnPanel, format!("{}/{}", self.path_to_desktops, path.clone()));
                let apps = parser::desktop_parser(self.settings.with_hide, self.path_to_desktops.clone());
                self.apps = apps.0;
                self.panel_apps = apps.1;
                Command::none()
            }
        }
    }
    
    fn subscription(&self) -> Subscription<Message> {
        subscription::events().map(Message::CloseMenu)
    }
    
    fn view(&self) -> Element<Message> {
        let main = modal(
            self.is_active_modal, 
            
            column!(
                row!(
                    text_input("Search...", &self.search_text)
                    .padding(9)
                    .width(Length::Fill)
                    .style(iced::theme::TextInput::Custom(Box::new(crate::themes::text_input::TextInputTheme)))
                    .on_input(Message::Search),
                    button(text("Settings")).on_press(Message::ShowSettings)
                ),
                match self.search_result.clone(){
                    Some(r) => {
                        MainContent::show_applications(r)
                    },
                    None => {
                        MainContent::show_applications( self.apps.clone())
                    }
                }
            )
            .width(Length::Fill)
            .height(Length::Fill)
            .padding(25)
            .spacing(35),
            container(
                column!(
                    row!(
                        toggler(String::from("Показывать скрытые"), self.settings.with_hide, |b| Message::ToggleWithHide(b))
                        .width(Length::Fill),
                    ),
                )
            )
            .width(Length::Fixed(600.0))
            .padding(25)
            .style(iced::theme::Container::Custom(Box::new(crate::themes::containers::ModalTheme)))
        )
        .on_esc(Message::HideSettings)
        .backdrop(Message::HideSettings);
            
        if self.is_hide{
            row!(
                button(text("View").horizontal_alignment(iced::alignment::Horizontal::Center))
                .height(Length::Fill)
                .on_press(Message::ShowManager)
                .style(iced::theme::Button::Custom(Box::new(crate::themes::buttons::MenuButtonTheme))),
                MainContent::show_applications_panel(self.panel_apps.clone())
                .width(Length::Fill)
                .height(Length::Fixed(50.0)),
            )
            .width(Length::Fill)
            
            .into()
        } else{
            container(main)
            .style(iced::theme::Container::Custom(Box::new(crate::themes::containers::ContainerTheme)))
            .width(Length::Fill)
            .height(Length::Fill)
            .into()
        }
    }
    fn style(&self) -> iced::theme::Application{
        iced::theme::Application::Custom(Box::new(crate::themes::application::ApplicationTheme))
    }
}