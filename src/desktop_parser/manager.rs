
use std::io::{BufReader, BufWriter,prelude::*, Error};
use std::fs::{OpenOptions,create_dir_all, read_dir,copy, remove_file, File};
use std::path::{PathBuf, Path};
use std::env::home_dir;
use cairo;
use rsvg;


pub enum Extensions{
    Png,
    Svg,
    Desktop
}
pub enum Toggle{    
    Hide,
    WithHide,
    OnPanel
}

pub fn convert_image(path_from: String, path_to: PathBuf){
    let paths = read_dir(&path_from).unwrap();
    for path in paths {
        if let Ok(entry) = path{
            if let Some(extension) = entry.path().extension() {
                if extension == "svg"{
                    if File::open(path_to
                        .clone()
                        .join(entry.path().file_name().unwrap().to_str().unwrap().replace("svg", "png"))
                    )
                    .is_ok(){
                        continue;
                    }
                    let handle = rsvg::Loader::new().read_path(&entry.path()).unwrap();
                    let renderer = rsvg::CairoRenderer::new(&handle);
                    let surface = cairo::ImageSurface::create(cairo::Format::ARgb32, 160,160).unwrap();
                    let cr = cairo::Context::new(&surface).unwrap();
                    let res = renderer.render_document(
                        &cr,
                        &cairo::Rectangle::new(0.0, 0.0, 160.0, 160.0),
                    );
                    let mut file = BufWriter::new(File::create(path_to.join(entry.path().file_name().unwrap().to_str().unwrap().replace("svg", "png"))).unwrap());
                    surface.write_to_png(&mut file).unwrap();
                    
                }
            }
        }   
    }
    

}

pub fn create_image_directory() -> PathBuf{
    let home = home_dir().unwrap();
    let path_to = home.join(".config/iced-appmanager/images/");
    create_dir_all(path_to.clone()).unwrap();
    convert_image("/usr/share/icons/hicolor/scalable/apps/".to_string(), path_to.clone());
    copy_files(Extensions::Png, "/var/lib/flatpak/exports/share/icons/hicolor/128x128/apps/".to_owned(), path_to.clone());
    convert_image("/var/lib/flatpak/exports/share/icons/hicolor/scalable/apps/".to_string(), path_to.clone());
    convert_image("/usr/share/pixmaps/".to_string(), path_to.clone());
    copy_files(Extensions::Png, "/usr/share/icons/hicolor/48x48/apps/".to_owned(), path_to.clone());
    copy_files(Extensions::Png, "/usr/share/icons/hicolor/scalable/apps/".to_owned(), path_to.clone());
    copy_files(Extensions::Png, "/usr/share/icons/".to_owned(), path_to.clone());
    copy_files(Extensions::Png, "/usr/share/pixmaps/".to_owned(), path_to.clone());
    path_to
}
pub fn delete_desktop_file(path: String){
    remove_file(path);
}

pub fn create_settings_file() -> PathBuf{
    let home = home_dir().unwrap();
    let path_to = home.join(".config/iced-appmanager/").join("settings.ini");
    if !File::open(path_to.clone()).is_ok(){
        File::create(&path_to.clone());   
    }
    path_to
}

pub fn create_config_dir() -> PathBuf{
    let home = home_dir().unwrap();
    let path_to = home.join(".config/iced-appmanager/apps/");
    create_dir_all(path_to.clone()).unwrap();
    copy_files(Extensions::Desktop, String::from("/var/lib/flatpak/exports/share/applications/"), path_to.clone());
    copy_files(Extensions::Desktop, String::from("/usr/share/applications/"), path_to.clone());
    path_to
}
pub fn copy_files(extension: Extensions, path_from: String, path_to: PathBuf){
    let extension_name = match extension {
        Extensions::Png => {
            "png"
        },
        Extensions::Svg => {
            "svg"
        },
        Extensions::Desktop => {
            "desktop"
        }
    };
    let paths = read_dir(&path_from).unwrap();
    for path in paths {
        if let Ok(entry) = path{
            if let Some(extension) = entry.path().extension(){
                if extension == extension{
                    if File::open(path_to.clone().join(entry.path().file_name().unwrap())).is_ok(){
                        continue;
                    }
                    let file = File::create(&path_to.clone().join(entry.path().file_name().unwrap())).unwrap();
                    copy(entry.path(), path_to.clone().join(entry.path().file_name().unwrap())).unwrap();
                }
            }
        }
    }
}

pub fn toggle_property(toggle: Toggle, path: String) -> Result<(),Error>{
    let toggle_elem = match toggle {
        Toggle::Hide => {
            "X-NoDisplay-iced"
        },
        Toggle::WithHide => {
            "WithHide"
        },
        Toggle::OnPanel => {
            "OnPanel"
        }
    };
    let mut file = OpenOptions::new().read(true).write(true).open(&path.clone()).unwrap();
    let reader = BufReader::new(&file);
    let mut lines = reader.lines().map(|l| l.unwrap()).collect::<Vec<String>>();
    for i in 0..lines.len() {
        if lines[i].contains(&format!("{}=false", toggle_elem)) {
            lines[i] = lines[i].replace(&format!("{}=false", toggle_elem), &format!("{}=true", toggle_elem));
            let file = File::create(&path.clone()).unwrap();
            let mut writer = BufWriter::new(&file);
            for line in &lines {
                writeln!(writer, "{}", line)?;
            }
            return Ok(());
        } 
        if lines[i].contains(&format!("{}=true", toggle_elem)){
            lines[i] = lines[i].replace(&format!("{}=true", toggle_elem), &format!("{}=false", toggle_elem));
            let file = File::create(&path.clone()).unwrap();
            let mut writer = BufWriter::new(&file);
            for line in &lines {
                writeln!(writer, "{}", line)?;
            }
            return Ok(());
        }
    }
    let mut file = OpenOptions::new().write(true).append(true).open(&path.clone()).unwrap();
    file.write_all(format!("{}=true\n", toggle_elem).as_bytes())?;
    Ok(())
}