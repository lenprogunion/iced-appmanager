use super::lexer::{Token, lexer};
use std::fs::read;
#[derive(Debug)]
pub struct Settings{
    pub with_hide: bool,
}
pub fn settings_parser(path: String) -> Settings{
    let file = read(&path);
    let data_from_file = String::from_utf8(file.unwrap()).unwrap();
    let tokens = lexer(data_from_file.clone());
    parse_tokens(tokens.clone())
}

fn parse_tokens(tokens: Vec<Token>) -> Settings{
    let mut with_hide = false;
    for token in tokens.iter(){
        match token{
            Token::Tag { name, value } => {
                if name.is_empty() || value.is_empty(){ continue; };
                match name.as_str(){
                    "WithHide" => {
                        if value.contains("true"){
                            with_hide = true;
                        }
                    },
                    _ => {
                        
                    }
                }
            },
            Token::Directive(name) => {
                
            }
        }
    }
    Settings{
        with_hide
    }
}

