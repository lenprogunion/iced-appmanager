use iced::{
    window, Font, Size,
};

use iced::{
    Settings,
    Application,
};

mod app_manager;
mod themes;
mod desktop_parser;

fn main() -> iced::Result{
    let width: i32 = 900;
    app_manager::init::AppManager::run(
        Settings{
            window: window::Settings{
                position: window::Position::Specific((1366 - width)/2, 766),
                decorations: false,
                max_size: Some((1366, 700)),
                size: (width as u32, 50),
                level: window::Level::AlwaysOnTop,
                transparent: true,
                resizable: false,
                ..window::Settings::default()
            },
            ..Settings::default()
        }
    )
}
