use iced::{
    color, 
    application::{Appearance, StyleSheet}, 
};

pub struct ApplicationTheme;

impl StyleSheet for ApplicationTheme {
    type Style = iced::Theme;
    
    fn appearance(&self, style: &Self::Style) -> Appearance {
        Appearance{
            background_color: iced::Color::TRANSPARENT,
            text_color: color!(255,255,255),
        }
    }
}



