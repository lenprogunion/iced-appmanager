use iced::widget::button::{Appearance, StyleSheet};
use iced::{
    color, 
    BorderRadius,
    
};
pub struct SideBarButtonTheme;

impl StyleSheet for SideBarButtonTheme {
    type Style = iced::Theme;
    
    fn active(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([22.0, 22.0, 22.0, 22.0]),
            background: Some(iced::Background::Color(iced::Color::TRANSPARENT)),
            ..Appearance::default()
        }
    }
    fn hovered(&self, style: &Self::Style) -> Appearance {
 
        Appearance{
            border_radius: BorderRadius::from([22.0, 22.0, 22.0, 22.0]),
            background: Some(iced::Background::Color(color!(100,100,100, 0.46))),
            ..Appearance::default()
        }
        
    }
    fn pressed(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([22.0, 22.0, 22.0, 22.0]),
            background: Some(iced::Background::Color(iced::Color::TRANSPARENT)),
            ..Appearance::default()
        }
    }
  
}


pub struct ElemButtonTheme;

impl StyleSheet for ElemButtonTheme {
    type Style = iced::Theme;
    
    fn active(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            background: Some(iced::Background::Color(iced::Color::TRANSPARENT)),
            ..Appearance::default()
        }
    }
    fn hovered(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            background: Some(iced::Background::Color(color!(255,255,255,0.3))),
            ..Appearance::default()
        }
        
    }
    fn pressed(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            background: Some(iced::Background::Color(color!(255,255,255,0.5))),
            ..Appearance::default()
        }
    }
  
}


pub struct MenuButtonTheme;

impl StyleSheet for MenuButtonTheme {
    type Style = iced::Theme;
    
    fn active(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            background: Some(iced::Background::Color(iced::Color::TRANSPARENT)),
            ..Appearance::default()
        }
    }
    fn hovered(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            background: Some(iced::Background::Color(color!(255,255,255,0.3))),
            ..Appearance::default()
        }
        
    }
    fn pressed(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            background: Some(iced::Background::Color(color!(255,255,255,0.5))),
            ..Appearance::default()
        }
    }
  
}