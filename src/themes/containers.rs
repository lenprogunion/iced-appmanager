use iced::widget::container::{Appearance, StyleSheet};
use iced::{
    color, 
    BorderRadius,
    
};
pub struct ContainerTheme;

impl StyleSheet for ContainerTheme {
    type Style = iced::Theme;
    
    fn appearance(&self, style: &Self::Style) -> Appearance {
        Appearance {
            text_color: Some(color!(44,44,44)),
            background: Some(iced::Background::Color(color!(220, 220, 220, 0.9))),
            border_radius: BorderRadius::from([22.0, 22.0, 22.0, 22.0]),
            border_width: 0.0,
            border_color: color!(0,0,0,0.0)
        }
    }
}

pub struct ContextMenuTheme;

impl StyleSheet for ContextMenuTheme {
    type Style = iced::Theme;
    
    fn appearance(&self, style: &Self::Style) -> Appearance {
        Appearance {
            text_color: Some(color!(44,44,44)),
            background: Some(iced::Background::Color(color!(220, 220, 220, 0.9))),
            border_radius: BorderRadius::from([22.0, 22.0, 22.0, 22.0]),
            border_width: 0.3,
            border_color: color!(250,250,250, 0.5)
        }
    }
}

pub struct ModalTheme;

impl StyleSheet for ModalTheme {
    type Style = iced::Theme;
    
    fn appearance(&self, style: &Self::Style) -> Appearance {
        Appearance {
            text_color: Some(color!(44,44,44)),
            background: Some(iced::Background::Color(color!(255, 255, 255, 0.8))),
            border_radius: BorderRadius::from([22.0, 22.0, 22.0, 22.0]),
            border_width: 0.3,
            border_color: color!(250,250,250, 0.5)
        }
    }
}