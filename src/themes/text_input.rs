use iced::{
    color, 
    BorderRadius,
};
use iced::widget::text_input::{StyleSheet, Appearance};


pub struct TextInputTheme;

impl StyleSheet for TextInputTheme {
    type Style = iced::Theme;
    fn active(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            background: iced::Background::Color(color!(255,255,255,0.4)),
            border_width: 1.0,
            border_color: color!(255,255,255,0.0),
            icon_color: color!(180, 180, 180)
        }
    }
    fn focused(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            border_width: 1.0,
            background: iced::Background::Color(color!(255,255,255,0.6)),
            border_color: color!(28, 113, 216,0.6),
            icon_color: color!(180, 180, 180)
        }
    }
    fn placeholder_color(&self, style: &Self::Style) -> iced::Color{
        color!(100, 100, 100)
    }
    fn value_color(&self, style: &Self::Style) -> iced::Color{
        color!(44, 44, 44)
    }
    fn disabled_color(&self, style: &Self::Style) ->  iced::Color{
        color!(180, 180, 180)
    }
    fn selection_color(&self, style: &Self::Style) ->  iced::Color{
        color!(255, 255, 255)
    }
    fn disabled(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            border_width: 1.0,
            background: iced::Background::Color(color!(28, 28, 28)),
            border_color: color!(255,255,255,0.0),
            icon_color: color!(180, 180, 180)
        }
    }
    fn hovered(&self, style: &Self::Style) -> Appearance {
        Appearance{
            border_radius: BorderRadius::from([12.0, 12.0, 12.0, 12.0]),
            background: iced::Background::Color(color!(255,255,255,0.4)),
            border_width: 1.0,
            border_color: color!(255,255,255,0.0),
            icon_color: color!(180, 180, 180)
        }
    }
  
}